#include<iostream>
using namespace std;
template<class result>
class Template{
   public:
       //int result=0;
       template<typename myDataType1>
       //myDataType1 result=0; -you cannot so you have to make result as mydayatype as a local variable
      
       void add(myDataType1 a, myDataType1 b){
           //myDataType1 result;
           result = a+b;
           cout<<"result ; "<<result<<endl;
           //giving a+b
       }
};
int main(){
   Template<int> obj;
   obj.add<int>(2,3);
   obj.add<float>(2.2,3.3);
}
